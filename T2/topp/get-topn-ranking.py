# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals, division
import sys, getopt
import random
import lib2to3

try:
    opts, args = getopt.getopt(sys.argv[1:], "", [])
except getopt.GetoptError:
    print("-s test_spk_utt_f score")

for_spk = False
for o, a in opts:
    print (o)

test_spk_utt_f = open(args[0])
score_f = open(args[1])

test_utt2spk = {}

for line in test_spk_utt_f:
    c = line.strip().split()
    for u in c[1:]:
        test_utt2spk.update({u: c[0]})

scores_per_utt = dict()
scores_utt_target = dict()
print("get " + str(len(test_utt2spk)) + " utts")
for line in score_f:
    c = line.strip().split()
    if c[1] not in test_utt2spk:
        print (c[1] + "not seen in utt2spk")
        continue

    if c[1] not in scores_per_utt:
        scores_per_utt.update({c[1]: []})

    if test_utt2spk[c[1]] == c[0]:
        scores_utt_target.update({c[1]: float(c[2])})

    scores_per_utt[c[1]].append(float(c[2]))

topn_f = open(args[1] + '.topn', 'w')
tops = []

temp = {}
sum_temp = 0
for u in scores_per_utt:
    if u not in scores_utt_target:
        print(u + " target score not found")
        continue
    tgt_score = scores_utt_target[u]
    scores_per_utt[u].sort(reverse=True)
    i = 0
    while i < len(scores_per_utt[u]):
        if tgt_score >= scores_per_utt[u][i]:
            break
        i += 1
    tops.append(i)
    topn_f.write(u + " " + str(i) + "\n")
    sum_temp = sum_temp + 1

    iii = str(i)
    if iii in temp.keys():
        temp[iii] = temp[iii] + 1
    else:
        temp[iii] = 1
topn_f.close()

top1 = 0.0
if str(0) in temp.keys():
    top1 = temp[str(0)] / float(sum_temp) * 100

top10_temp = 0
for j in temp.keys():
    if int(j) <= 9:
        top10_temp = top10_temp + temp[j]

top10 = top10_temp / float(sum_temp) * 100

print("平均排名: %f" % (float(sum(tops)) / len(tops) + 1.0))
print("Top1 概率 :%f%%" % top1)
print("Top10 概率 :%f%%" % top10)
