#!/usr/bin/python
import sys
from subprocess import call
import os
'''
This script checks the false rejection errors given a threshold,
and print the respective error counts of speakers
'''
trial_f = open(sys.argv[1])
score_f = open(sys.argv[2])
thres = float(sys.argv[3])

if len(sys.argv) < 5:
    target_analysis_fn=sys.argv[2].replace('_scores', '-analysis-target')
    nontarget_analysis_fn=sys.argv[2].replace('_scores', '-analysis-nontarget')
    target_scores_fn=sys.argv[2].replace('_scores', '-scores-target')
    nontarget_scores_fn=sys.argv[2].replace('_scores', '-scores-nontarget')

else:
    target_analysis_fn = sys.argv[4]
    nontarget_analysis_fn = sys.argv[5]


target_answer = set()
nontarget_answer = set()

# Format:  spk utt target/nontarget
for line in trial_f:
    content = line.strip().split()
    if content[2] == 'target':
        target_answer.add((content[0], content[1]))
    else:
        nontarget_answer.add((content[0], content[1]))

# print target_answer
# print nontarget_answer
target_error_count={}
target_all_count={}

nontarget_error_count={}
nontarget_all_count={}

target_scores_f = open(target_scores_fn,'w')
nontarget_scores_f = open(nontarget_scores_fn,'w')
# Format: spk utt score
for line in score_f:
    content=line.strip().split()
    # print  content
    if content[0] not in target_error_count:
        target_error_count.update({content[0]:0})
        target_all_count.update({content[0]:0})

    if (content[0], content[1]) in target_answer:
        target_scores_f.write(line)
        target_all_count[content[0]]=target_all_count[content[0]]+1
        if float(content[2]) < thres:
            target_error_count[content[0]]+=1

    if content[0] not in nontarget_error_count:
        nontarget_error_count.update({content[0]:0})
        nontarget_all_count.update({content[0]:0})

    if (content[0], content[1]) in nontarget_answer:
        nontarget_all_count[content[0]]=nontarget_all_count[content[0]]+1
        nontarget_scores_f.write(line)
        if float(content[2]) >= thres:
            nontarget_error_count[content[0]]+=1

target_scores_f.close()
nontarget_scores_f.close()
os.system("sort -k 3 -g "+target_scores_fn+"> temp; mv temp "+target_scores_fn)
os.system("sort -r -k 3 -g "+nontarget_scores_fn+"> temp; mv temp "+nontarget_scores_fn)
print len(target_error_count)
print len(nontarget_error_count)


target_analysis_f=open(target_analysis_fn,'w')
nontarget_analysis_f=open(nontarget_analysis_fn,'w')


error_rate={}
for i in target_all_count:
    if target_all_count[i]==0:
        error_rate[i]=0
    else:
        error_rate[i] = float(target_error_count[i])/target_all_count[i]

result=error_rate.items()

s=sorted(result,key= lambda p: p[1])
for a in s:
    target_analysis_f.write(a[0]+"\t"+str(a[1])+"\t"+str(target_error_count[a[0]])+"\t"+str(target_all_count[a[0]])+"\n")
    

error_rate={}
for i in nontarget_all_count:
    if nontarget_all_count[i] == 0:
        error_rate[i] = 0
    else:
        error_rate[i] = float(nontarget_error_count[i])/nontarget_all_count[i]

result=error_rate.items()

s=sorted(result, key=lambda p: p[1], reverse=True)
for a in s:
    nontarget_analysis_f.write(a[0]+"\t"+str(a[1])+"\t"+str(nontarget_error_count[a[0]])+"\t"+str(nontarget_all_count[a[0]])+"\n")
