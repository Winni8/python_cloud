# -*- coding: utf-8 -*-

"""
@author VoiceAI
@desc VPR声纹注册验证批量测试,,  这个是1：1交叉比对 together
@date 20180928

@usage
    >> python vpr_batch_test.py model_short_cn_dnn ./wav ./output_model_short_cn_dnn ./report_model_short_cn_dnn 16000

    args:
        model_short_cn_dnn		16K算法模型
        ./wav	                测试文件路径，内部结构为：[[对象文件夹] -> [register][verify]]，register为注册源文件夹，verify为验证源文件夹，详见例子
        ./output_model_short_cn_dnn	结果分数输出路径
        ./report_model_short_cn_dnn	简单测试报告输出路径
        16000 采样率
"""
from __future__ import absolute_import, unicode_literals, division, print_function
import datetime
import logging
import os
import sys
import time
import uuid

try:
    reload(sys)
    sys.setdefaultencoding('utf-8')
except:
    pass
from pyvoiceai import *

# 日志级别，可打印 DEBUG
V_LOG_LEVEL = logging.INFO

# 日志输出
logging.basicConfig(level=V_LOG_LEVEL,
                    format="%(asctime)s-%(filename)s,[%(name)s:%(funcName)s:%(lineno)d] [%(levelname)s]:%(message)s")
logger = mylog()

# 测试类型
V_TEST_TYPE = "VoicePrint Regconition 1：N"

# 配置信息
# 声纹云API地址
V_BASE_URL = "https://192.168.0.100:28072"
# V_BASE_URL = "https://127.0.0.1:8072"
V_APP_ID = "000eacad8a56440fac5f0b8aed07ab48"
V_APP_SECRET = "222eacad8a56440fac5f0b8aed07ab48"

# 从命令行读取
V_MODEL_TYPE = sys.argv[1]
V_WAV_ROOT = sys.argv[2]
V_RESULT_OUTPUT_ROOT = sys.argv[3]
V_REPORT_FILE_ROOT = sys.argv[4]
V_SRC_SAMPLE_RATE = int(sys.argv[5])


# 辅助类
class TesterInfo(object):
    def __init__(self, path, client_id, register_file_list, verify_file_list):
        self.path = path
        self.client_id = client_id
        self.register_file_list = register_file_list
        self.verify_file_list = verify_file_list


class VPR_1_n_Tester(object):
    """
        VPR 1:N 测试
    """

    def __init__(self):

        self.last_need_refresh_time = time.time()
        self.access_token = ""

        self.time_start = time.time()
        dt = datetime.datetime.fromtimestamp(self.time_start)
        self.genTime = dt.strftime('%Y%m%d-%H%M%S-%f')

        self.time_end = time.time()
        self.result_path = ""
        self.result_tag_path = ""

        self.target_list = []

        self.reportFile = None
        self.reportFilePath = ""

        self.uploadCountSum = 0
        self.uploadTimeSum = 0

        self.registerFailedCountSum = 0
        self.register_count_sum = 0
        self.registerTimeSum = 0

        self.verifyCountSum = 0
        self.verifyTimeSum = 0

        self.eer_target = list()
        self.eer_nottarget = list()

        self.threshold, self.eer = -1.0, -1.0

    def begin(self):
        try:
            if not os.path.exists(V_REPORT_FILE_ROOT):
                os.makedirs(V_REPORT_FILE_ROOT)
        except Exception as e:
            logger.error(e)
            return False

        self.reportFilePath = os.path.join(V_REPORT_FILE_ROOT, 'test_' + V_MODEL_TYPE + '_report_%s.txt' % self.genTime)
        self.reportFile = open(self.reportFilePath, 'wb')

        logger.info("日志报告保存于:%s", self.reportFilePath)
        try:
            _V_WAV_ROOT = V_WAV_ROOT.decode('gb2312')
        except:
            _V_WAV_ROOT = V_WAV_ROOT

        self.log("****************************************************************************")
        self.log("VoiceAI VoiceCloud Test Report")
        self.log("****************************************************************************")
        self.log("")
        self.log("")

        self.log("测试模型：%s" % V_MODEL_TYPE)
        self.log("测试数据：%s" % _V_WAV_ROOT)
        self.log("采样率：%d" % V_SRC_SAMPLE_RATE)

        dt = datetime.datetime.fromtimestamp(self.time_start)
        self.log("测试开始：%s" % dt.strftime('%Y-%m-%d %H:%M:%S.%f'))
        self.time_start = time.time()

        return True

    def end(self):
        self.time_end = time.time()
        dt = datetime.datetime.fromtimestamp(self.time_end)
        self.log("测试结束：%s" % dt.strftime('%Y-%m-%d %H:%M:%S.%f'))
        # self.log("测试生成的client Id：%s" % self.target_list)
        self.log("总上传次数：%d" % self.uploadCountSum)
        self.log("总上传时间：%f 秒" % self.uploadTimeSum)
        if self.uploadCountSum > 0:
            self.log("平均上传时间：%f 秒" % (self.uploadTimeSum / float(self.uploadCountSum)))
        else:
            self.log("平均上传时间：0 秒")

        self.log("总注册失败次数：%d" % self.registerFailedCountSum)
        self.log("总注册次数：%d" % self.register_count_sum)
        self.log("总注册时间：%f 秒" % self.registerTimeSum)
        if self.register_count_sum > 0:
            self.log("平均注册时间：%f 秒" % (self.registerTimeSum / float(self.register_count_sum)))
        else:
            self.log("平均注册时间：0 秒")
        self.log("总对比次数：%d" % self.verifyCountSum)
        # self.log("总对比时间：%f 秒" % (self.verifyTimeSum / 1000000.0))
        # if self.verifyCountSum > 0:
        #     self.log("平均对比时间：%f 秒" % ((self.verifyTimeSum / 1000000.0) / float(self.verifyCountSum)))
        # else:
        #     self.log("平均对比时间：0 秒")
        self.log("测试总耗时：%f 秒" % (self.time_end - self.time_start))
        self.log("当前阈值Threshold：%f" % self.threshold)
        self.log("等错误率EER：{:0.2%}".format(self.eer))
        self.log("测试结果score输出路径：")
        self.log(self.result_path)
        self.log("测试结果target输出路径：")
        self.log(self.result_tag_path)
        self.log("")
        self.log("****************************************************************************")
        self.log("Powered by VoiceAI")
        self.log("****************************************************************************")

        self.reportFile = None

    def compute_eer(self):

        target_scores = sorted(self.eer_target)
        nontarget_scores = sorted(self.eer_nottarget)

        logger.info("*" * 10)
        logger.info("以下为eer结果:")
        logger.info("target:%s", target_scores)
        logger.info("nottarget:%s", nontarget_scores)
        logger.info("*" * 10)

        target_size = len(target_scores)
        nontarget_size = len(nontarget_scores)

        if target_size == 0 or nontarget_size == 0:
            logger.error("can not compare eer")
            return -1, -1
        else:
            target_position = 0
            for target_position in range(target_size):
                nontarget_n = nontarget_size * target_position * 1.0 / target_size
                nontarget_position = int(nontarget_size - 1 - nontarget_n)
                if nontarget_position < 0:
                    nontarget_position = 0
                if nontarget_scores[nontarget_position] < target_scores[target_position]:
                    # print "nontarget_scores[nontarget_position] is",  nontarget_position, nontarget_scores[nontarget_position]
                    # print "target_scores[target_position] is",  target_position, target_scores[target_position]
                    break

            threshold = target_scores[target_position]
            eer = float(target_position * 1.0 / target_size)
            return threshold, eer

    def log(self, content):
        mylog(1).info(content)
        self.reportFile.write((content + "\n").encode("utf-8", "ignore"))

    def refresh_auth_token(self):
        """
        刷新token
        """
        now = time.time()

        if self.last_need_refresh_time == 0 or self.last_need_refresh_time - now < 200:
            logger.warning("刷新token = %s", self.access_token)
            api = AppAuthAPI(V_BASE_URL, V_APP_ID, access_token=self.access_token)
            api.app_auth_token_refresh()
            self.last_need_refresh_time = time.time() + api.get_expires()

    def vpr_sdk_api_test_register(self, access_token, client_id, file_path_list_for_register):
        """
        声纹注册
        """
        register_count_sum = self.register_count_sum
        try:
            # 声纹上传，注册，验证
            api = AppVoicePrintAPI(V_BASE_URL, V_APP_ID, access_token)

            # 声纹上传
            _t1_upload = time.time()
            api.app_voiceprint_upload(client_id, file_path_list_for_register)
            _t2_upload = time.time()
            self.uploadCountSum += 1
            self.uploadTimeSum += (_t2_upload - _t1_upload)
            register_file_id_list = api.get_file_id_list()
            if len(register_file_id_list) == 0:
                self.register_count_sum += 1
                self.registerFailedCountSum += 1
                logger.error("声纹文件上传失败")
                return

            # 声纹注册
            _t1 = time.time()
            self.refresh_auth_token()
            flag_bool_response, err, data = api.app_voiceprint_register(client_id, V_SRC_SAMPLE_RATE, V_MODEL_TYPE,
                                                                        register_file_id_list)
            _t2 = time.time()
            self.register_count_sum += 1
            self.registerTimeSum += (_t2 - _t1)
            if (not flag_bool_response) or (data is None) or ("result" not in data) or (data["result"] is False):
                logger.error("注册失败：%s,%s,%s", flag_bool_response, data, err['errormsg'])
        except Exception as e:
            if register_count_sum == self.register_count_sum:
                self.register_count_sum += 1
                self.registerFailedCountSum += 1
            logger.error(e)

    def vpr_sdk_api_test_together(self, access_token, group_id, client_id, target_client_id, file_path_list_for_verify):
        """
        自己与他人1:N验证
        """
        api = AppVoicePrintAPI(V_BASE_URL, V_APP_ID, access_token)

        result = []
        # 声纹验证
        for i in range(len(file_path_list_for_verify)):
            self.refresh_auth_token()
            # 声纹上传
            try:
                __path = file_path_list_for_verify[i]
                _t1_upload = time.time()
                api.app_voiceprint_upload(client_id, [__path])
                _t2_upload = time.time()
                self.uploadCountSum += 1
                self.uploadTimeSum += (_t2_upload - _t1_upload)

                verify_fileid_list = api.get_file_id_list()
                if len(verify_fileid_list) == 0:
                    logger.error("上传失败(0x02)：%s,%s", client_id, [__path])
                    continue

                flag_bool_response, err, data = api.app_voiceprint_verify(group_id, target_client_id, V_SRC_SAMPLE_RATE,
                                                                          V_MODEL_TYPE,
                                                                          verify_fileid_list)

                non_matching_list = []
                matching_list = []
                if "non_matching_list" in data and data["non_matching_list"] is not None:
                    non_matching_list = data["non_matching_list"]

                if "matching_list" in data and data["matching_list"] is not None:
                    matching_list = data["matching_list"]

                # logger.info(data)

                self.verifyCountSum += data["spk_info_size"]
                result.append(
                    (os.path.splitext(os.path.basename(__path))[0], data["value"], data["threshold"],
                     matching_list + non_matching_list))
            except Exception as e:
                logger.error(e)

        return result

    def vpr_sdk_api_create_tmp_user_and_test(self, access_token, group_id, gen_int_time):
        """
            创建临时用户并测试
        """
        client_api = AppClientAPI(V_BASE_URL, V_APP_ID, access_token)

        tester_info_dict = dict()
        tester_info_list = []

        # 列出文件夹下所有的目录与文件
        dir_list = os.listdir(V_WAV_ROOT)
        logger.info("声纹音频待测试文件夹：%s", dir_list)

        # 声纹文件上传和注册
        # 每个文件夹，三个register文件注册声纹
        for i in range(len(dir_list)):
            self.refresh_auth_token()
            _path = os.path.join(V_WAV_ROOT, dir_list[i])
            if dir_list[i].startswith(".") or not os.path.isdir(_path):
                continue

            dir_register = os.path.join(_path, "register")
            dir_verify = os.path.join(_path, "verify")
            if not os.path.exists(dir_register) or not os.path.exists(dir_verify):
                logger.warning("测试文件夹下%s 或 %s 找不到", dir_register, dir_verify)
                continue

            register_file_list = []
            verify_file_list = []

            dir_register_list = os.listdir(dir_register)
            for ii in range(len(dir_register_list)):
                __path = os.path.join(dir_register, dir_register_list[ii])
                if dir_register_list[ii].startswith(".") or not __path.endswith(".wav") and not __path.endswith(".WAV"):
                    continue
                register_file_list.append(__path)

            if len(dir_register_list) == 0:
                continue

            dir_verify_list = os.listdir(dir_verify)
            for ii in range(len(dir_verify_list)):
                __path = os.path.join(dir_verify, dir_verify_list[ii])
                if dir_verify_list[ii].startswith(".") or not __path.endswith(".wav") and not __path.endswith(".WAV"):
                    continue
                verify_file_list.append(__path)

            if len(verify_file_list) == 0:
                continue

            # 创建临时用户，每个文件夹一个用户
            temp_client_name = "temp_client_%s_%s" % (dir_list[i], gen_int_time)
            flag_bool_response, err, client_id = client_api.app_client_create(temp_client_name, "api测试专用", group_id)

            if flag_bool_response is False:
                logger.error("创建临时用户失败:%s,%s", temp_client_name, err["errorms"])
                continue

            logger.info("创建临时用户成功:%s", temp_client_name)
            logger.info("register_file_list:%s", register_file_list)
            logger.info("verify_file_list:%s", verify_file_list)

            # 用户ID群记录
            self.target_list.append(client_id)

            #  批量声纹注册
            self.vpr_sdk_api_test_register(access_token, client_id, register_file_list)

            tester_info = TesterInfo(dir_list[i], client_id, register_file_list, verify_file_list)
            tester_info_list.append(tester_info)
            tester_info_dict[client_id] = tester_info

        # 新建记录文件记录 音频根路径，待验证音频，匹配值，是否命中
        result_path = os.path.join(V_RESULT_OUTPUT_ROOT, 'test_' + V_MODEL_TYPE + '_result_%s_scores.txt' % self.genTime)
        result_output = open(result_path, 'wb')
        result_tag_path = os.path.join(V_RESULT_OUTPUT_ROOT,
                                       'test_' + V_MODEL_TYPE + '_result_%s_tag.txt' % self.genTime)
        result_tag_output = open(result_tag_path, 'wb')

        # 这里进行交叉1:N验证
        try:
            for i in range(len(tester_info_list)):
                self.refresh_auth_token()
                tester_info = tester_info_list[i]

                # 每个文件下的verify开始与全组匹配
                _result = self.vpr_sdk_api_test_together(access_token, group_id, tester_info.client_id, "",
                                                         tester_info.verify_file_list)

                for m in range(len(_result)):
                    logger.info("匹配结果：%s", _result[m])
                    self.refresh_auth_token()
                    all_list = _result[m][3]
                    if all_list is None:
                        continue

                    for n in range(len(all_list)):
                        info = all_list[n]
                        _client_id = info["clientid"]
                        if _client_id not in tester_info_dict:
                            continue

                        result_tester_info = tester_info_dict[_client_id]

                        # c是client3
                        # c可能匹配到client1,client2
                        # 可能写入
                        # todo bug here maybe
                        score = info["value"]
                        label = "nontarget"
                        if tester_info.client_id == _client_id:
                            label = "target"
                            self.eer_target.append(score)
                        else:
                            self.eer_nottarget.append(score)

                        c_score = "%s %s %f\n" % (result_tester_info.path, _result[m][0], score)
                        c_tag = "%s %s %s\n" % (result_tester_info.path, _result[m][0], label)
                        result_output.write(c_score.encode("utf-8", "ignore"))
                        result_tag_output.write(c_tag.encode("utf-8", "ignore"))
        except Exception as e:
            logger.error(e)

        # 临时用户删除
        if len(self.target_list) > 0:
            self.refresh_auth_token()
            ok, err, _ = client_api.app_client_delete(self.target_list)
            if ok:
                logger.info("删除用户:%s", self.target_list)
            else:
                logger.error("删除用户失败:%s, %s", self.target_list, err)

        # eer
        self.threshold, self.eer = self.compute_eer()
        # result_output.write(("###%f,%f" % (self.threshold, self.eer)).encode("utf-8", "ignore"))
        result_output.close()
        result_tag_output.close()
        return result_path, result_tag_path

    def vpr_sdk_api_test(self):
        """
            测试主入口
        :return:
        """

        try:
            if not os.path.exists(V_RESULT_OUTPUT_ROOT):
                os.makedirs(V_RESULT_OUTPUT_ROOT)
        except Exception as e:
            logger.error(e)
            return

        # 1. 验证，如果预先填写好access_token，则刷新
        api = AppAuthAPI(V_BASE_URL, V_APP_ID)

        api.app_auth_get(V_APP_SECRET)
        api.app_auth_token_get()
        self.access_token = api.get_access_token()

        if len(self.access_token) == 0:
            logger.error("认证失败!")
            exit(1)

        # 2. 刷新token
        self.refresh_auth_token()

        # 3. 创建临时分组
        group_api = AppGroupAPI(V_BASE_URL, V_APP_ID, self.access_token)

        # python2/python3
        try:
            gen_int_time = uuid.uuid1().get_hex()
        except:
            gen_int_time = uuid.uuid1()

        group_name = "temp_group_%s" % gen_int_time
        flag_bool_response, err, group_id = group_api.app_group_create(group_name, "temp_group")
        if flag_bool_response and group_id != 0:
            logger.info("创建临时用户组:%s", group_name)
            self.result_path, self.result_tag_path = self.vpr_sdk_api_create_tmp_user_and_test(self.access_token,
                                                                                               group_id,
                                                                                               gen_int_time)
            # 4 删除临时分组
            ok, err, _ = group_api.app_group_delete([group_id])
            if ok:
                logger.info("删除用户组:%s", group_id)
            else:
                logger.error("删除用户组失败:%s, %s", group_id, err)
        else:
            logger.error("创建临时用户组失败：%s,%s", group_name, err["errormsg"])

        # 删除token
        api.app_auth_token_remove()


if __name__ == "__main__":
    if len(sys.argv) > 2:
        logger.info("sys.argv:%s", sys.argv)

    tester = VPR_1_n_Tester()
    if not tester.begin():
        logger.error("tester.begin() is False")
        exit(1)
    try:
        tester.vpr_sdk_api_test()
    except Exception as e:
        logger.error("vpr_sdk_api_test error:%s", e)

    tester.end()

    exit(0)
