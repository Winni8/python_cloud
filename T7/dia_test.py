# -*- coding: utf-8 -*-

"""
@author VoiceAI
@desc DIA人声分割批量测试
@date 20180928

@usage
    >> python dia_test.py  model_common_short ./wav ./output_model_common_short ./report_model_common_short 8000

    args:
        model_common_short		算法模型
        ./wav	                测试文件路径
        ./output_model_common_short	结果输出路径
        ./report_model_common_short	简单测试报告输出路径
        8000 采样率
"""
from __future__ import absolute_import, unicode_literals, division, print_function
import datetime
import logging
import os
import sys
import shutil

try:
    reload(sys)
    sys.setdefaultencoding('utf-8')
except:
    pass
import time
import uuid

from pyvoiceai import *

# 日志级别，可打印 DEBUG
V_LOG_LEVEL = logging.INFO
# 日志输出
logging.basicConfig(level=V_LOG_LEVEL,
                    format="%(asctime)s-%(filename)s,[%(name)s:%(funcName)s:%(lineno)d] [%(levelname)s]:%(message)s")

logger = mylog()

V_BASE_URL = "https://192.168.0.100:28072"
# V_BASE_URL = "https://127.0.0.1:8072"
V_APP_ID = "000eacad8a56440fac5f0b8aed07ab48"
V_APP_SECRET = "222eacad8a56440fac5f0b8aed07ab48"
V_MODEL_TYPE = sys.argv[1]
V_WAV_ROOT = sys.argv[2]
V_RESULT_OUTPUT_ROOT = sys.argv[3]
V_REPORT_OUTPUT_ROOT = sys.argv[4]
V_PARMS = sys.argv[5]


class DIATest:
    def __init__(self, api_url, app_id, app_secret, model_type="model_short_cn_dnn", wav_dir="./wav",
                 report_root="./report", result_root="./output", src_sample_rate=16000, group_id="", client_id="",
                 file_id_list=[]):
        self.auth = None
        self.token = ""
        self.api_url = api_url
        self.app_id = app_id
        self.app_secret = app_secret
        self.last_need_refresh_time = 0
        self.src_sample_rate = src_sample_rate
        self.model_type = model_type

        self.wav_dir = wav_dir
        self.file_id_list = file_id_list

        self.group_id = group_id
        self.client_id = client_id
        self.report_root = report_root
        self.result_root = result_root

        self.reportFilePath = None
        self.reportFile = None

        self.time_start = time.time()
        self.time_end = time.time()

        self.uploadCountSum = 0
        self.uploadErrCountSum = 0
        self.uploadTimeSum = 0

        self.verifyCountSum = 0
        self.verifyErrCountSum = 0

    def log(self, content):
        mylog(1).info(content)
        self.reportFile.write((content + "\n").encode("utf-8", "ignore"))

    def refresh_token(self):
        if self.auth is None:
            self.auth = AppAuthAPI(self.api_url, self.app_id)
            self.auth.app_auth_get(self.app_secret)
            self.auth.app_auth_token_get()
            self.token = self.auth.get_access_token()
        else:
            now = time.time()
            if self.last_need_refresh_time == 0 or self.last_need_refresh_time - now < 200:
                self.auth.app_auth_token_refresh()
                self.last_need_refresh_time = time.time() + self.auth.get_expires()

    def remove_token(self):
        if self.auth is None:
            pass
        else:
            self.auth.app_auth_token_remove()
            self.auth = None

    def create_group(self):
        if self.group_id == "":
            self.refresh_token()
            # python2/python3
            try:
                gen_int_time = uuid.uuid1().get_hex()
            except:
                gen_int_time = uuid.uuid1()
            group_name = "temp_asv_group_%s" % gen_int_time

            group_api = AppGroupAPI(self.api_url, self.app_id, self.token)
            flag_bool_response, err, group_id = group_api.app_group_create(group_name, "asv temp_group")
            if flag_bool_response and group_id != 0:
                self.group_id = group_id
                logger.info("创建临时用户组:%s" % group_name)
            else:
                logger.error("创建临时用户组失败:%s,%s" % (group_name, err["errormsg"]))
                return False
        return True

    def remove_group(self):
        if self.group_id != "":
            self.refresh_token()
            group_api = AppGroupAPI(self.api_url, self.app_id, self.token)
            ok, _, _ = group_api.app_group_delete([self.group_id])
            if ok:
                logger.info("删除用户组:%s" % self.group_id)
                self.group_id = ""

    def create_client(self):
        if self.client_id == "" and self.group_id != "":
            self.refresh_token()

            try:
                gen_int_time = uuid.uuid1().get_hex()
            except:
                gen_int_time = uuid.uuid1()

            client_name = "temp_asv_client_%s" % gen_int_time

            client_api = AppClientAPI(self.api_url, self.app_id, self.token)
            flag_bool_response, err, client_id = client_api.app_client_create(client_name, "asv temp_client",
                                                                              self.group_id)
            if flag_bool_response and client_id != 0:
                self.client_id = client_id
                logger.info("创建临时用户:%s" % client_name)
            else:
                logger.error("创建临时用户失败:%s,%s" % (client_name, err["errormsg"]))
                return False
        return True

    def remove_client(self):
        if self.client_id != "":
            self.refresh_token()
            client_api = AppClientAPI(self.api_url, self.app_id, self.token)
            ok, _, _ = client_api.app_client_delete([self.client_id])
            if ok:
                logger.info("删除用户:%s" % self.client_id)
                self.client_id = ""

    def prepare_group_and_user(self):
        if self.create_group() is False:
            return False
        if self.create_client() is False:
            return False
        return True

    def clean_group_and_user(self):
        self.remove_client()
        self.remove_group()
        self.remove_token()

    @staticmethod
    def list_files(root_dir, suffix='.wav', isall=False, iscur=False):
        file = []
        for parent, dirnames, filenames in os.walk(root_dir):
            if parent == root_dir:
                for filename in filenames:
                    if filename.endswith(suffix):
                        if isall:
                            file.append(root_dir + '/' + filename)
                        else:
                            file.append(filename)
                if not iscur:
                    return file
            else:
                if iscur:
                    for filename in filenames:
                        if filename.endswith(suffix):
                            if isall:
                                file.append(root_dir + '/' + filename)
                            else:
                                file.append(filename)
                else:
                    pass
        return file

    def upload_file(self, file_path):
        if len(self.file_id_list) > 0:
            return True
        self.refresh_token()
        api = AppVoicePrintAPI(self.api_url, self.app_id, self.token)

        try:
            _t1_upload = time.time()
            ok, err, data = api.app_voiceprint_upload(self.client_id, [file_path])
            if ok is False:
                raise Exception("upload flag false %s, err: %s" % (file_path, err["errormsg"]))
            _t2_upload = time.time()
            self.uploadCountSum += 1
            self.uploadTimeSum += (_t2_upload - _t1_upload)
        except Exception as e:
            self.uploadErrCountSum += 1
            logger.error(e)
            return False

        self.file_id_list = api.get_file_id_list()
        logger.info("upload file:%s, file ids:%s" % (file_path, self.file_id_list))
        if len(self.file_id_list) == 0:
            logger.error("upload error get file ids empty:%s" % file_path)
            return False
        return True

    def create_output(self):

        time_start = time.time()
        dt = datetime.datetime.fromtimestamp(time_start)
        gen_time = dt.strftime('%Y%m%d-%H%M%S-%f')

        try:
            if not os.path.exists(self.report_root):
                os.makedirs(self.report_root)
            self.reportFilePath = os.path.join(self.report_root,
                                               'test_' + self.model_type + '_report_%s.txt' % gen_time)
            self.reportFile = open(self.reportFilePath, 'wb')

            if not os.path.exists(self.result_root):
                os.makedirs(self.result_root)
        except Exception as e:
            logger.error(e)
            return False
        return True

    def dia(self):
        file_paths = self.list_files(self.wav_dir, '.wav', True)
        if len(file_paths) == 0:
            logger.warning("wav empty")
            return False

        if self.create_output() is False:
            logger.error("create dir err")
            return False

        self.log("人声分割开始")
        self.log("测试模型：%s" % self.model_type)
        self.log("测试数据：%s" % self.wav_dir)
        dt = datetime.datetime.fromtimestamp(self.time_start)
        self.log("测试开始：%s" % dt.strftime('%Y-%m-%d %H:%M:%S.%f'))
        self.time_start = time.time()

        self.refresh_token()
        if self.token == "":
            logger.error("token empty")
            return False

        api = AppVoicePrintAPI(self.api_url, self.app_id, self.token)

        for i in file_paths:
            flag = True
            sub_dir = i.replace(".", "")
            real_dir = sub_dir.split("/")[-1]
            save_dir = "%s/%s" % (self.result_root, real_dir)

            save_dir_lock = save_dir + ".lock"
            if os.path.exists(save_dir_lock) is True:
                logger.error("dir lock exist:%s, will ignore it!!!" % save_dir)
                continue
            if os.path.exists(save_dir) is True:
                logger.error("dir already exist:%s, auto rm it!!!" % save_dir)
                shutil.rmtree(save_dir)

            os.makedirs(save_dir)
            logger.info("file : %s will dia save in %s" % (i, save_dir))
            self.file_id_list = []
            if self.upload_file(i) is False or len(self.file_id_list) <= 0:
                self.log("%s upload err:" % i)
                continue

            self.refresh_token()
            _t1_upload = time.time()
            self.refresh_token()
            ok, err, task_id = api.app_voiceprint_dialaunch(self.group_id, self.client_id, self.src_sample_rate,
                                                            self.model_type,
                                                            self.file_id_list[0])

            if ok is False:
                logger.error("%s,%s" % (i, err['errormsg']))
                self.verifyErrCountSum += 1
                continue
            logger.info("task_id start: %s" % task_id)

            times = 0
            while True:
                times = times + 1
                if times > 30:
                    self.verifyErrCountSum += 1
                    logger.error("file:%s,test_id:%s too many times" % (i, task_id))
                    flag = False
                    break
                self.refresh_token()
                ok, err, data = api.app_voiceprint_dia_task_list_one(self.group_id, self.client_id, task_id)
                if ok is False:
                    self.verifyErrCountSum += 1
                    logger.error(err)
                    flag = False
                    break

                if "task_info" in data.keys():
                    if int(data["task_info"]["last_update_time"]) >= 60:
                        self.verifyErrCountSum += 1
                        logger.error("file:%s,test_id:%s too many times 60s" % (i, task_id))
                        flag = False
                        break

                if "result_list" in data.keys() and data["result_list"] is not None:
                    _t2_upload = time.time()
                    self.verifyCountSum += 1

                    for jj in data["result_list"]:
                        file_idid = jj["result_id"]
                        logger.info("download file:%s" % file_idid)

                        self.refresh_token()
                        ok, raw = api.app_voiceprint_dia_result_download(file_idid)
                        if ok is False:
                            self.verifyErrCountSum += 1
                            logger.error("file:%s,test_id:%s download fail:%s" % (i, task_id, file_idid))
                            flag = False
                            break
                        try:
                            open("%s/%s.wav" % (save_dir, file_idid), "wb").write(raw)
                            logger.info("file:%s,test_id:%s save good:%s" % (i, task_id, file_idid))
                        except:
                            self.verifyErrCountSum += 1
                            logger.error("file:%s,test_id:%s save fail:%s" % (i, task_id, file_idid))
                            flag = False
                            break
                    break
                logger.info("remain: %s times,sleep 5 s" % (30 - times))
                time.sleep(5)

            if flag is True:
                # lock
                try:
                    logger.info("write lock:%s" % save_dir_lock)
                    open(save_dir_lock, "wb").write("xx".encode("utf-8", "ignore"))
                    logger.info("add lock done:%s" % save_dir_lock)
                except Exception as e:
                    logger.error("write lock error:%s" % e)
        self.time_end = time.time()
        dt = datetime.datetime.fromtimestamp(self.time_end)
        self.log("测试结束：%s" % dt.strftime('%Y-%m-%d %H:%M:%S.%f'))
        self.log("总成功上传次数：%d" % self.uploadCountSum)
        self.log("总失败上传次数：%d" % self.uploadErrCountSum)
        self.log("总成功上传时间：%f 秒" % self.uploadTimeSum)
        if self.uploadCountSum > 0:
            self.log("平均成功上传时间：%f 秒" % (self.uploadTimeSum / float(self.uploadCountSum)))
        else:
            self.log("平均成功上传时间：0 秒")

        self.log("总成功请求检验次数：%d" % self.verifyCountSum)
        self.log("总失败请求检验次数：%d" % self.verifyErrCountSum)
        self.log("result File in:%s" % self.result_root)
        self.log("report File in:%s" % self.reportFilePath)
        self.log("测试总耗时：%f 秒" % (self.time_end - self.time_start))
        return True


if __name__ == "__main__":
    dia = DIATest(V_BASE_URL, V_APP_ID, V_APP_SECRET, V_MODEL_TYPE, V_WAV_ROOT, V_REPORT_OUTPUT_ROOT,
                  V_RESULT_OUTPUT_ROOT, int(V_PARMS))
    if dia.prepare_group_and_user() is False:
        dia.clean_group_and_user()
        exit(1)
    dia.dia()
    dia.clean_group_and_user()
