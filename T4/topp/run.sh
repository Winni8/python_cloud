#!/bin/bash
#### 第一步: 设置配置 ####

# thres 设置为想要实验的阈值  与eer一起的阈值
# score_dir 文件所在的目录
# tagfile 标记文件
# scorefile 分数文件

# analyze会产生一系列文件 例如:
# $scorefile-scores-nontarget:	非同一个人得分从高到低排，可以快速找出错误接受的情况
# $scorefile-scores-target: 同一个人得分从低到高排，可以快速找出错误拒绝的情况
# $scorefile-analysis-nontarget：容易误接受的说话人，第二个值为比例，第三个值为误接受次数，第四个值为涉及他的nontarget比对数
# $scorefile-analysis-target：容易误拒绝的说话人，第二个值为比例，第三个值为误拒绝次数，第四个值为涉及他的target比对数
###################################################
spk2utt=spk2utt
thres=217.77
score_dir=./input
scorefile=test_model_short_cn_dnn_result_20181012-180713-857756_scores.txt
tagfile=test_model_short_cn_dnn_result_20181012-180713-857756_tag.txt
###################################################


#### 第二步： 计算中间产物 ####
python analyze.py $score_dir/$tagfile $score_dir/$scorefile $thres


#### 第三步： 计算Top1/Top2 ####
#对每一条语音，获取最像的说话人, -n 表示top2
python get-most-likely-one.py $score_dir/$scorefile $score_dir/top1
python get-most-likely-one.py -n 2 $score_dir/$scorefile $score_dir/top2
sort $score_dir/top1 > $score_dir/top1.sorted
sort $score_dir/top2 > $score_dir/top2.sorted


#### 第四步： 生成中间文件 ####
# spk2utt
python spk2utt-tool.py $score_dir/$scorefile $score_dir/$spk2utt

#### 第五步： 计算TOPN ####
# 获得每条语音对应说话人的排位
python get-topn-ranking.py $score_dir/$spk2utt $score_dir/$scorefile







