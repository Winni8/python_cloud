#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

'''
This script gen middle file spk2utt
'''

# python spk2utt-tool.py score.txt spk.txt
score_f = open(sys.argv[1], "rb")
spk_f = open(sys.argv[2], "wb")

m = {}
result = {}
for line in score_f:
    content = line.strip().split()
    audio = content[1]
    m[audio] = []

score_f.close()

for audio in m:
    if "_" in audio:
        id = audio.split("_")[0]
    elif "-" in audio:
        id = audio.split("-")[0]
    else:
        raise Exception("file must contain _ or -")

    if id in result.keys():
        result[id].append(audio)
    else:
        result[id] = [audio]

for i in result.keys():
    # print i, result[i]
    spk_f.write("%s %s\n" % (i, " ".join(result[i])))

spk_f.close()
