import sys, getopt

#assume the scores are sorted by utt
num_utts=dict()
threshold=dict()
minus_thres=False
spk_list=set()

try:  
	opts, args = getopt.getopt(sys.argv[1:], "s:u:n:",[])  
except getopt.GetoptError:  
	print "-u num_utt -s spk_list -n top_n scores "
num_utts_fn=""
spk_list_fn=""
n=1

for o,a in opts:
	if o in ("-u"):
		num_utts_fn=a
	
	if o in ("-n"):
		n=int(a)
                print n

	if o in ("-s"):
		spk_list_fn=a
	

if num_utts_fn!="":
	flexible_scoring=True

	#num_utts.ark

	num_utts_f=open(num_utts_fn)
	for line in num_utts_f:
		c=line.strip().split()
		num_utts.update({c[0]:c[1]})
else:
	flexible_scoring=False

	'''
	if len(args)>3:
		threshold_f=open(args[3])
		for line in threshold_f:
			c=line.strip().split(':')
			threshold.update({c[0]:c[1]})
		
			minus_thres=True
	'''		

if spk_list_fn!="":
	spk_list_f=open(spk_list_fn)
	for line in spk_list_f:
		spk_list.add(line.strip())

#print len(args)
top_scores=dict()
min_scores=dict()
#print len(num_utts)

#print args[0]+"\t"+args[1]
last_utt=''
utt_scores=[]
scores=open(args[0])
for line in scores:
	content=line.strip().split()	
	cur_utt=content[1]
	cur_spk=content[0]
	cur_score=float(content[2])

	#if flexible_scoring==True and int(num_utts[content[0]]) <20 or ( content[0] not in spk_list and spk_list_fn!=""):
#		print "skip"
		#continue

	#if minus_thres==True:
	#	if int(num_utts[content[0]]) >10:
	#		cur_score -= -12-(int(num_utts[content[0]])-10)*1
	#	else:
	#		cur_score -= float(threshold[num_utts[content[0]]])

	if cur_utt==last_utt:
		utt_scores.append( (cur_spk, cur_score) )

	elif last_utt!='' and last_utt!=cur_utt:
	#	print "utt_score="+str( len(utt_scores))
		top_scores.update({last_utt:[]})
		utt_scores.sort(key=lambda p:p[1], reverse=True)
		for i in range (0, n):
			top_scores[last_utt].append(utt_scores[i])
                        
		utt_scores=[]
		utt_scores.append( (cur_spk, cur_score) )

	last_utt=cur_utt

if  len(utt_scores)!=0:
	top_scores.update({last_utt:[]})
	utt_scores.sort( key=lambda p:p[1], reverse=True)

	for i in range (0, n):
		top_scores[last_utt].append(utt_scores[i])

#print (len(top_scores))
f=open(args[1],'w')
for item in top_scores:
	f.write( item+" "+" ".join( x[0] +'|'+str(x[1])    for x in top_scores[item]) +'\n')
	

	
